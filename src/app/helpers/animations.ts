import { trigger, style, transition, animate, query, stagger, keyframes, animation, useAnimation } from "@angular/animations";

const pageAnimationParams = animation(
    [
        style({
            opacity: '{{opacity}}',
            transform: 'scale({{scale}}) translate3d({{x}}, {{y}}, {{z}})'
        }),
        animate('{{duration}} {{delay}} cubic-bezier(0.0, 0.0, 0.2, 1)', style('*'))
    ],
    {
        params: {
            duration: '200ms',
            delay: '0ms',
            opacity: '0',
            scale: '1',
            x: '0',
            y: '0',
            z: '0'
        }
    }
);

export const pageAnimation = trigger('pageAnimation', [transition('void => *', [useAnimation(pageAnimationParams)])]);

export const listAnimation = trigger('listAnimation', [
    transition('* <=> *', [
        query(':enter',
            [style({
                opacity: 0, transform: 'translateY(-30px)'
            }), stagger('90ms',
                animate('300ms ease-out', style({ opacity: 1, transform: 'translateY(0)' })))],
            { optional: true }
        ),
        query(':leave',
            animate('200ms', style({
                opacity: 0,
                transform: 'translateX(-30px)'
            })),
            { optional: true }
        )
    ])
]);
export const bounceInOut = trigger('bounceInOut', [
    transition(':enter',
        animate('.4s',
            keyframes([
                style({ transform: 'scale3d(0.6, 0.6, 0.6)', offset: 0 }),
                style({ transform: 'scale3d(1.4, 1.4, 1.4)', offset: 0.2 }),
                style({ transform: 'scale3d(1.2, 1.2, 1.2)', offset: 0.4 }),
                style({ transform: 'scale3d(1.06, 1.06, 1.06)', offset: 0.6 }),
                style({ transform: 'scale3d(1, 1, 1)', offset: 0.8 }),
            ])
        )
    ),
    transition(':leave',
        animate('.4s',
            keyframes([
                style({ transform: 'scale3d(1, 1, 1)', offset: 0 }),
                style({ transform: 'scale3d(1.06, 1.06, 1.06)', offset: 0.2 }),
                style({ transform: 'scale3d(1.2, 1.2, 1.2)', offset: 0.4 }),
                style({ transform: 'scale3d(1.4, 1.4, 1.4)', offset: 0.6 }),
                style({ transform: 'scale3d(0.6, 0.6, 0.6)', offset: 0.8 })
            ]),
        )
    ),
]);