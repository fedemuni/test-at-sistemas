import { FormGroup } from "@angular/forms";

export class Helper {
    static compareWith(prop: string, c1: any, c2: any): boolean {
        return c1 && c2 ? c1[prop] === c2[prop] : c1 === c2;
    }
    static isValidForm(form: FormGroup) {
        form.markAllAsTouched();
        return form.valid;
    }
}