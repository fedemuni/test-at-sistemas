import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToolbarService {
  private mode: BehaviorSubject<'MAIN' | 'SECONDARY'> = new BehaviorSubject<'MAIN' | 'SECONDARY'>(null);
  mode$ = this.mode.asObservable();

  private title: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  title$ = this.title.asObservable();

  constructor() { }

  setTitle(title: string) {
    this.title.next(title);
    this.mode.next('SECONDARY');
  }

  setToMainMode() {
    this.mode.next('MAIN');
    this.title.next('Películas');
  }
}
