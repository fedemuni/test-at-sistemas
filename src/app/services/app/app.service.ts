import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, forkJoin, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FullMovie, Movie } from '../../models/movies';
import { Actor } from '../../models/actors';
import { map, switchMap } from 'rxjs/operators';
import { Company } from 'src/app/models/companies';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private movies: BehaviorSubject<Movie[]> = new BehaviorSubject<Movie[]>([]);
  movies$ = this.movies.asObservable();

  private actors: BehaviorSubject<Actor[]> = new BehaviorSubject<Actor[]>([]);
  actors$ = this.actors.asObservable();

  private companies: BehaviorSubject<Company[]> = new BehaviorSubject<Company[]>([]);
  companies$ = this.companies.asObservable();

  constructor(
    private httpClient: HttpClient
  ) { }

  getMovies(): void {
    this.httpClient.get<Movie[]>(`${environment.http.url}/movies`).subscribe(data => {
      this.movies.next(data);
    });
  }

  getMovieById(id: number): Observable<FullMovie> {
    return this.httpClient.get<Movie>(`${environment.http.url}/movies/${id}`)
      .pipe(
        switchMap(movie => {
          return forkJoin(movie.actors.map(actor => this.getActorById(actor)))
            .pipe(
              map(actors => {
                return {
                  ...movie,
                  actors
                }
              })
            )
        })
      );
  }

  createMovie(movie: Movie): Observable<Movie> {
    return this.httpClient.post<Movie>(`${environment.http.url}/movies`, movie);
  }

  updateMovie(movieId: number, movie: Movie): Observable<Movie> {
    return this.httpClient.put<Movie>(`${environment.http.url}/movies/${movieId}`, movie);
  }

  deleteMovie(id: number): Observable<Movie> {
    return this.httpClient.delete<Movie>(`${environment.http.url}/movies/${id}`)
      .pipe(
        map(movie => {
          this.movies.next([]);
          return movie;
        })
      );
  }

  getActors() {
    return this.httpClient.get<Actor[]>(`${environment.http.url}/actors`).subscribe(data => {
      this.actors.next(data);
    });
  }

  getCompamies() {
    return this.httpClient.get<Company[]>(`${environment.http.url}/companies`).subscribe(data => {
      this.companies.next(data);
    });
  }

  getActorById(id: number): Observable<Actor> {
    return this.httpClient.get<Actor>(`${environment.http.url}/actors/${id}`);
  }

}
