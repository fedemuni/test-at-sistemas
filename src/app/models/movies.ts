import { Actor } from "./actors";

export interface Movie {
    id?: number;
    title: string;
    poster: string;
    genre: string[];
    year: number;
    duration: number;
    imdbRating: number;
    actors: number[];
}

export interface FullMovie extends Omit<Movie, 'actors'> {
    actors: Actor[];
}
