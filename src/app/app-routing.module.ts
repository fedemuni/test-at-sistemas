import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './layouts/pages/pages.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent, children: [
      { path: '', redirectTo: 'movies', pathMatch: 'full' },
      { path: 'movies', loadChildren: () => import('./modules/movies/movies.module').then(m => m.MoviesModule) },
    ]
  }
  //{ path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
