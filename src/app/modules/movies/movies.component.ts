import { Component, OnInit } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { Router } from '@angular/router';
import { listAnimation } from 'src/app/helpers/animations';
import { AppService } from 'src/app/services/app/app.service';
import { ToolbarService } from 'src/app/services/toolbar.service';


@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss'],
  animations: [listAnimation]
})
export class MoviesComponent implements OnInit {
  viewCol: number = 25;
  constructor(
    public appService: AppService,
    public mediaObserver: MediaObserver,
    private router: Router,
    private toolbarService: ToolbarService
  ) { }

  ngOnInit(): void {
    this.appService.getMovies();
    this.toolbarService.setToMainMode();
  }

  addMovie() {
    this.router.navigate(['/movies/handle/add']);
  }

}
