import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MoviesComponent } from './movies.component';

const routes: Routes = [
  { path: '', component: MoviesComponent },
  { path: ':id', loadChildren: () => import('../movie-detail/movie-detail.module').then(m => m.MovieDetailModule) },
  { path: 'handle', loadChildren: () => import('../add-edit-movie/add-edit-movie.module').then(m => m.AddEditMovieModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule { }
