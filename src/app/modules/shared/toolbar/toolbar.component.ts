import { Location } from '@angular/common';
import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToolbarService } from 'src/app/services/toolbar.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, OnDestroy {
  /**
   * Contains subscriptions to destroy and prevent memory leaks
   */
  private destroy$: Subject<void> = new Subject<void>();
  private history: string[] = []
  @Output() onMenuIconClick: EventEmitter<any> = new EventEmitter<any>();
  constructor(
    public toolbarService: ToolbarService,
    private location: Location,
    private router: Router
  ) { }

  ngOnInit() {
    this.router.events
      .pipe(takeUntil(this.destroy$)).subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.history.push(event.urlAfterRedirects)
        }
      })
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  sidenavToggle() {
    this.onMenuIconClick.emit();
  }

  back(): void {
    this.history.pop();
    if (this.history.length > 0) {
      this.location.back()
    } else {
      this.router.navigateByUrl('/')
    }
  }
}