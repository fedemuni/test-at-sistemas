import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, Observable, Subject } from 'rxjs';
import { map, switchMap, takeUntil } from 'rxjs/operators';
import { pageAnimation } from 'src/app/helpers/animations';
import { FullMovie, Movie } from 'src/app/models/movies';
import { AppService } from 'src/app/services/app/app.service';
import { ToolbarService } from 'src/app/services/toolbar.service';
@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss'],
  animations: [pageAnimation]
})
export class MovieDetailComponent implements OnInit, OnDestroy {
  /**
   * Contains subscriptions to destroy and prevent memory leaks
   */
  private destroy$: Subject<void> = new Subject<void>();
  movie: FullMovie;
  constructor(
    private activatedRoute: ActivatedRoute,
    private appService: AppService,
    private router: Router,
    private toolbarService: ToolbarService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe(params => {
        this.initData(params['id']);
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  deleteMovie() {
    this.appService.deleteMovie(this.movie.id)
      .pipe(takeUntil(this.destroy$))
      .subscribe(_ => {
        this.router.navigate(['movies']);
      })
  }

  editMovie() {
    this.router.navigate([`movies/handle/edit/${this.movie.id}`]);
  }

  private initData(movieId: number) {
    this.appService.getMovieById(movieId)
      .subscribe((data: FullMovie) => {
        this.movie = data;
        this.toolbarService.setTitle(this.movie.title);
      });
  }

}
