import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MovieDetailComponent } from './movie-detail.component';
import { MaterialModule } from '../material/material.module';

const routes: Routes = [
  { path: '', component: MovieDetailComponent, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    MovieDetailComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(routes),
  ]
})
export class MovieDetailModule { }
