import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddEditMovieRoutingModule } from './add-edit-movie-routing.module';
import { AddEditMovieComponent } from './add-edit-movie.component';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AddEditMovieComponent
  ],
  imports: [
    CommonModule,
    AddEditMovieRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class AddEditMovieModule { }
