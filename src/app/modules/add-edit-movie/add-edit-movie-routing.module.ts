import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEditMovieComponent } from './add-edit-movie.component';

const routes: Routes = [
  { path: 'add', component: AddEditMovieComponent },
  { path: 'edit/:id', component: AddEditMovieComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddEditMovieRoutingModule { }
