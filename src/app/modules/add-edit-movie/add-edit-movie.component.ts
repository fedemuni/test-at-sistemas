import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { bounceInOut, pageAnimation } from 'src/app/helpers/animations';
import { Helper } from 'src/app/helpers/helper';
import { Actor } from 'src/app/models/actors';
import { FullMovie, Movie } from 'src/app/models/movies';
import { AppService } from 'src/app/services/app/app.service';
import { ToolbarService } from 'src/app/services/toolbar.service';

@Component({
  selector: 'app-add-edit-movie',
  templateUrl: './add-edit-movie.component.html',
  styleUrls: ['./add-edit-movie.component.scss'],
  animations: [pageAnimation, bounceInOut]
})
export class AddEditMovieComponent implements OnInit {
  /**
   * Contains subscriptions to destroy and prevent memory leaks
   */
  private destroy$: Subject<void> = new Subject<void>();
  movieForm: FormGroup;
  movie: FullMovie;
  constructor(
    private activatedRoute: ActivatedRoute,
    public appService: AppService,
    private fb: FormBuilder,
    private router: Router,
    private toolbarService: ToolbarService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.initData();
    this.activatedRoute.params
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe(params => {
        if (params['id']) {
          this.initEditData(params['id']);
        } else {
          this.toolbarService.setTitle('Nueva película');
        }
      });
  }

  addChip(currentValue: string, field: string) {
    if (currentValue) {
      let values: string[] = this.movieForm.get(field)?.value;
      if (!values.some(val => val === currentValue)) {
        values.push(currentValue);
      }
    }
  }

  removeChip(currentValue: string, field: string) {
    let values: string[] = this.movieForm.get(field)?.value;
    values.splice(values.indexOf(currentValue), 1);
  }

  compareFn(c1: any, c2: any): boolean {
    return Helper.compareWith('id', c1, c2);
  }

  onSubmit() {
    if (Helper.isValidForm(this.movieForm)) {
      if (this.movie) {
        this.appService.updateMovie(this.movie.id, this.mapRequest())
          .pipe(takeUntil(this.destroy$))
          .subscribe(_ => {
            this.router.navigate(['movies']);
          });
      } else {
        this.appService.createMovie(this.mapRequest())
          .pipe(takeUntil(this.destroy$))
          .subscribe(_ => {
            this.router.navigate(['movies']);
          });
      }
    }
  }

  private initEditData(movieId: number) {
    this.appService.getMovieById(movieId)
      .subscribe((movie: FullMovie) => {
        this.movie = movie;
        this.toolbarService.setTitle(this.movie.title);
        this.setDefaultValues(movie);
      });
  }

  private initData() {
    this.appService.getActors();
    this.appService.getCompamies();
  }

  private initForm() {
    this.movieForm = this.fb.group({
      title: [null, Validators.required],
      poster: [],
      genre: [[]],
      actors: [[]],
      actorsInput: [],
      studio: [[]],
      year: [],
      duration: [],
      imdbRating: [],
      genreInput: []
    })
  }

  private setDefaultValues(movie: FullMovie) {
    this.movieForm.patchValue({
      title: movie.title,
      poster: movie.poster,
      genre: movie.genre,
      actors: movie.actors,
      studio: null,
      year: movie.year,
      duration: movie.duration,
      imdbRating: movie.imdbRating
    });
  }

  private mapRequest(): Movie {
    return {
      actors: this.movieForm.get('actors')?.value?.map((actor: Actor) => actor.id),
      title: this.movieForm.get('title').value,
      duration: this.movieForm.get('duration').value,
      genre: this.movieForm.get('genre').value,
      imdbRating: this.movieForm.get('imdbRating').value,
      poster: this.movieForm.get('poster').value,
      year: this.movieForm.get('year').value
    }
  }

}
